<?php

namespace Request;

class GenericRequest
{
    public function requestGeneric ($filename, $context=null)
    {
        $result = array();                  // $use_include_path = false
        $result['content'] = file_get_contents($filename, false, $context);
        $result['header'] = $http_response_header;
        return $result;
    }
}

class HttpRequest extends GenericRequest
{
    protected $_host = '';

    protected function prepareHeader ()
    {
        $header = array(
            'Accept' => '*/*',
            'Accept-Language' => 'de',
        );
        return $header;
    }

    protected function finishHeader ($header)
    {
        $temp = array();
        if(is_array($header))
        foreach($header as $k=>$v) {
            $temp[] = "$k: $v\r\n";
        }
        return implode('', $temp);
    }

    protected function finishCookie ($cookies)
    {
        $temp = array();
        if(is_array($cookies))
        foreach($cookies as $k=>$v) {
            $temp[] = "$k=$v";
        }
        return implode('; ', $temp);
    }

    public function requestHTTP ($url, $options)
    {
        return $this->requestGeneric($this->_host.$url, stream_context_create($options));
    }

    public function parseCookies ($header)
    {
        $cookies = array();
        foreach($header as $line) {
            $part1 = explode(':', $line);
            if($part1[0] != 'Set-Cookie') continue;
            $part2 = explode(';', $part1[1]);
            $part3 = explode('=', trim($part2[0]));
            $cookies[$part3[0]] = $part3[1];
        }
        return $cookies;
    }

    public function __construct ($host='')
    {
        $this->_host = $host;
    }
}

class PostRequest extends HttpRequest
{
    public function requestPostJSON ($url, $cookie, $data_array)
    {
        $header = $this->prepareHeader();
        $header['Cookie'] = $this->finishCookie($cookie);
        $data = json_encode($data_array);
        if($data) {
            $header['Content-type'] = 'application/json';
            $header['Content-Length'] = strlen($data);
        } else {
            $data = null;
            $header['Content-Length'] = 0;
        }
        $header = $this->finishHeader($header);
        echo "header: $header\n";
        echo "data: $data\n";
        $options = array(
            'http' => array(
                'header'  => $header,
                'method'  => 'POST',
                'content' => $data,
            )
        );
        $response = $this->requestHTTP($url, $options);
        $response['cookies'] = $this->parseCookies($response['header']);
        return $response;
    }

    public function requestPostString ($url, $cookie, $data_string)
    {
        $header = $this->prepareHeader();
        $header['Cookie'] = $this->finishCookie($cookie);
        $data = $data_string;
        if($data) {
            $header['Content-type'] = 'application/x-www-form-urlencoded';
            $header['Content-Length'] = strlen($data);
        } else {
            $data = null;
            $header['Content-Length'] = 0;
        }
        $header = $this->finishHeader($header);
        echo "header: $header\n";
        echo "data: $data\n";
        $options = array(
            'http' => array(
                'header'  => $header,
                'method'  => 'POST',
                'content' => $data,
            )
        );
        $response = $this->requestHTTP($url, $options);
        $response['cookies'] = $this->parseCookies($response['header']);
        return $response;
    }

    public function requestPost ($url, $cookie, $data)
    {
        $header = $this->prepareHeader();
        $header['Cookie'] = $this->finishCookie($cookie);
        if($data) {
            $data = http_build_query($data);
            $header['Content-type'] = 'application/x-www-form-urlencoded';
            $header['Content-Length'] = strlen($data);
        } else {
            $data = null;
            $header['Content-Length'] = 0;
        }
        $header = $this->finishHeader($header);
        echo "header: $header\n";
        echo "data: $data\n";
        $options = array(
            'http' => array(
                'header'  => $header,
                'method'  => 'POST',
                'content' => $data,
            )
        );
        $response = $this->requestHTTP($url, $options);
        $response['cookies'] = $this->parseCookies($response['header']);
        return $response;
    }

    public function request ($url, $cookie, $data)
    {
        return $this->requestPost($url, $cookie, $data);
    }

    public function __construct ($host)
    {
        parent::__construct($host);
    }
}

class GetRequest extends HttpRequest
{
    public function requestGet ($url, $cookie, $data='')
    {
        $header = $this->prepareHeader();
        $header['Cookie'] = $this->finishCookie($cookie);
        if($data) {
            $url .= '?'.http_build_query($data);
        }
        $header = $this->finishHeader($header);
        echo "header: $header\n";
        echo "url: $url\n";
        $options = array(
            'http' => array(
                'header'  => $header,
                'method'  => 'GET',
            )
        );
        $response = $this->requestHTTP($url, $options);
        $response['cookies'] = $this->parseCookies($response['header']);
        return $response;
    }

    public function request ($url, $cookie, $data='')
    {
        return $this->requestGet($url, $cookie, $data);
    }

    public function __construct ($host)
    {
        parent::__construct($host);
    }
}

// Beispiel:
// Bevor eine Anfrage gesendet werden kann, muss zunächst ein Objekt der Klasse
// GetRequest oder PostRequest erzeugt werden. Dem Konstruktor wird dabei das
// Schema und der Hostname übergeben.
// $request_objekt = new Request\GetRequest('http://www.example.com');

// Anschließend können mit dem zuvor erzeugten $request_obj Anfragen an den
// Server gestellt werden, dessen Hostname beim Erzeugen angegeben wurde.
// Bei der Anfrage werden als Parameter der aufzurufende Pfad, die zu verwendenden
// Cookies und ggf. zu übertragende Daten angegeben. Wenn keine Cookies verwendet
// werden sollen, muss null angegeben werden!
// $result = $request_obj->request('/index.html', null);


