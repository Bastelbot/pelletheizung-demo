<?php

require('request.php');
$cfg = require('config.php');
$get = new Request\GetRequest('http://'.$cfg['host']);
$post = new Request\PostRequest('http://'.$cfg['host']);

// hier beginnt das eigentliche programm

// login
echo "start part1 -- login\n";
$resp1 = $post->requestPost(
    '/index.cgi',
    null,                       // keine cookies zum Anmelden
    array(
        'username'=>$cfg['user'],
        'password'=>$cfg['pass'],
        'language'=>'de',
    )
);
echo "part1 finished.\n";

$cookies = $resp1['cookies'];
$cookies['language'] = 'de';    // Patch, da aus irgendeinem Grund nur 'd' anstatt 'de' zurückgeliefert wird
echo "cookies: "; print_r($cookies);

/*
// follow redirect
echo "start part2 -- follow redirect\n";
$resp2 = $get->requestGet('/', $cookies);
print_r($resp2);
echo "part2 finished.\n";
*/

/*
// language definitions
echo "start part3 -- loading language definitions\n";
$resp3 = $get->requestGet('/lang/language.cgi', $cookies);
file_put_contents('language.js', $resp3['content']);
echo "part3 finished.\n";
*/

echo "start part4 -- retrieve values\n";
$parameter = include('parameter.php');
foreach($parameter as &$param) { $param = '"'.$param.'"'; }
$resp4 = $post->requestPostString(
    '/?action=get&attr=1',
    $cookies,
    '['.implode(',', $parameter).']=""'
);
print_r($resp4);
echo "part4 finished\n";

// parameter und werte ausgeben
$json = json_decode($resp4['content']);
file_put_contents('output.json', json_encode($json, JSON_PRETTY_PRINT));

