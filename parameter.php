<?php

return array(
    "CAPPL:FA[0].asche_minimum_laufzeit_asche",
    "CAPPL:FA[0].asche_aschedauer",
    "CAPPL:FA[0].asche_nachlaufzeit_aschebox",

    "CAPPL:FA[0].ausgang_motor[0]",
    "CAPPL:FA[0].ausgang_motor[1]",
    "CAPPL:FA[0].ausgang_motor[2]",
    "CAPPL:FA[0].ausgang_motor[3]",
    "CAPPL:FA[0].ausgang_motor[5]",
    "CAPPL:FA[0].ausgang_motor[7]",
    "CAPPL:FA[0].ausgang_motor[8]",
    "CAPPL:FA[0].ausgang_motor[9]",
    "CAPPL:FA[0].ausgang_motor[11]",
    "CAPPL:FA[0].ausgang_stoermelderelais",

    "CAPPL:FA[0].betriebsart_fa",

    "CAPPL:FA[0].L_agt_zuend_flammueb",
    "CAPPL:FA[0].L_alterkessel",
    "CAPPL:FA[0].L_anzahl_zuendung",

    "CAPPL:FA[0].L_br1",
    "CAPPL:FA[0].L_brennerstarts",
    "CAPPL:FA[0].L_brennerlaufzeit_anzeige",
    "CAPPL:FA[0].L_bsk_status",

    "CAPPL:FA[0].L_drehzahl_ascheschnecke_ist",
    "CAPPL:FA[0].L_drehzahl_uw_ist",

    "CAPPL:FA[0].L_einschublaufzeit",

    "CAPPL:FA[0].L_feuerraumtemperatur",
    "CAPPL:FA[0].L_feuerraumtemperatur_soll",

    "CAPPL:FA[0].L_kap_sensor_raumentnahme",
    "CAPPL:FA[0].L_kap_sensor_zwischenbehaelter",
    "CAPPL:FA[0].L_kesselstatus",
    "CAPPL:FA[0].L_kesseltemperatur",
    "CAPPL:FA[0].L_kesseltemperatur_soll_anzeige",

    "CAPPL:FA[0].L_luefterdrehzahl",

    "CAPPL:FA[0].L_mittlere_laufzeit",

    "CAPPL:FA[0].L_pausenzeit",

    "CAPPL:FA[0].L_saugzugdrehzahl",
    "CAPPL:FA[0].L_sillstandszeit",

    "CAPPL:FA[0].L_unterdruck",
    "CAPPL:FA[0].L_unterdruck_soll_anzeige",

    "CAPPL:FA[0].leistung_brennstoffkorrektur",

    "CAPPL:FA[0].rm_reinigungszeit1_befuellung",
    "CAPPL:FA[0].rm_saugzeit2_ein",

    "CAPPL:LOCAL.anlage_betriebsart",
    "CAPPL:LOCAL.fernwartung_einheit",

    "CAPPL:LOCAL.hk[0].aktives_zeitprogramm",
    "CAPPL:LOCAL.hk[0].alias",
    "CAPPL:LOCAL.hk[0].betriebsart[1]",
    "CAPPL:LOCAL.hk[0].raumtemp_absenken",
    "CAPPL:LOCAL.hk[0].raumtemp_heizen",

    "CAPPL:LOCAL.L_aussentemperatur_ist",

    "CAPPL:LOCAL.L_fernwartung_datum_zeit_sek",

    "CAPPL:LOCAL.L_hk[0].mode_switch_enabled",
    "CAPPL:LOCAL.L_hk[0].raumtemp_ist",
    "CAPPL:LOCAL.L_hk[1].mode_switch_enabled",
    "CAPPL:LOCAL.L_hk[2].mode_switch_enabled",
    "CAPPL:LOCAL.L_hk[3].mode_switch_enabled",
    "CAPPL:LOCAL.L_hk[4].mode_switch_enabled",
    "CAPPL:LOCAL.L_hk[5].mode_switch_enabled",

    "CAPPL:LOCAL.L_weather_clouds",
    "CAPPL:LOCAL.L_weather[0]",

    "CAPPL:LOCAL.L_ww[0].ausschaltfuehler_ist",
    "CAPPL:LOCAL.L_ww[0].einschaltfuehler_ist",
    "CAPPL:LOCAL.L_ww[0].temp_soll",

    "CAPPL:LOCAL.L_zaehler_fehler",

    "CAPPL:LOCAL.pellematic_vorhanden[0]",
    "CAPPL:LOCAL.weather_config",

    "CAPPL:LOCAL.ww[0].aktives_zeitprogramm",
    "CAPPL:LOCAL.ww[0].alias",
    "CAPPL:LOCAL.ww[0].betriebsart[1]",
    "CAPPL:LOCAL.ww[0].einmal_aufbereiten",
    "CAPPL:LOCAL.ww[0].temp_absenken",
    "CAPPL:LOCAL.ww[0].temp_heizen",
);

